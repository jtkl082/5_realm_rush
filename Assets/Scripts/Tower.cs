﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    // Parameter of each tower
    [SerializeField] Transform ObjectToPan;
    [SerializeField] float attackRange = 30f;
    // [SerializeField] GameObject[] weapon; // couldn't get to work
    [SerializeField] ParticleSystem ProjectileParticles;

    // State of each tower
    Transform targetEnemy;

    public Waypoint baseWaypoint; // what tower is standing on


    // Update is called once per frame
    void Update()
    {
        SetTargetEnemy();
        if (targetEnemy)
        {
            TowerLookAt();
            TowerShoot();
        }
        else
        {
            SetTurretsActive(false);
        }
        
    }

    private void SetTargetEnemy()
    {
        var sceneEnemies = FindObjectsOfType<EnemyDamage>();
        if (sceneEnemies.Length == 0) { return; }

        Transform closestEnemy = sceneEnemies[0].transform;

        foreach (EnemyDamage testEnemy in sceneEnemies)
        {
            closestEnemy = GetClosestEnemy(closestEnemy, testEnemy.transform);
        }

        targetEnemy = closestEnemy;
    }

    private Transform GetClosestEnemy(Transform transformA, Transform transformB)
    {
        var distToA = Vector3.Distance(transform.position, transformA.position);
        var distToB = Vector3.Distance(transform.position, transformB.position);

        if (distToA < distToB)
        {
            return transformA;
        }
        return transformB;
    }

    private void TowerLookAt()
    {
        ObjectToPan.LookAt(targetEnemy);
    }

    private void TowerShoot()
    {
        float dist = Vector3.Distance(targetEnemy.position, ObjectToPan.position);
        // print(gameObject + "Distance to other: " + dist);
        if (dist <= attackRange)
        {
            SetTurretsActive(true);
        }
        else
        {
            SetTurretsActive(false);
        }
    }      
  
    private void SetTurretsActive(bool isActive)
    {
        var emissionModule = ProjectileParticles.emission;
        emissionModule.enabled = isActive;
        /* foreach (GameObject bullets in weapon)
        {
            var emissionModule = ObjectToPan.GetComponent<ParticleSystem>().emission;
            emissionModule.enabled = isActive;
        }
        */
    }
}
