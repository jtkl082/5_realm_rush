﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{
    [Range(0.1f, 120f)]
    [SerializeField] float secondsBetweenSpawn = 2f;
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] Transform enemyParent;
    [SerializeField] Text spawnedEnemies;
    [SerializeField] AudioClip spawnedEnemiesSFX;

    int score;

    private void Start()
    {
        {
            StartCoroutine(SpawnEnemies());
            spawnedEnemies.text = score.ToString();
        }
    }

    IEnumerator SpawnEnemies()
    {
        while (true) // run forever
        {
            AddScore();
            GetComponent<AudioSource>().PlayOneShot(spawnedEnemiesSFX);

            var enemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
            enemy.transform.parent = enemyParent;
            yield return new WaitForSeconds(secondsBetweenSpawn);
        }
    }

    private void AddScore()
    {
        score++;
        spawnedEnemies.text = score.ToString();
    }
}
