﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    // public ok here as a data class
    public bool isExplored = false;
    public Waypoint exploredFrom;
    public bool isPlaceable = true;

    [SerializeField] Color exploredColor;

    Vector2Int gridPos;

    const int gridSize = 10;
        
    private void Update()
    {
        // SetIsExploredColor(); optional color test
    }

    public int GetGridSize()
    {
        return gridSize;
    }

    public Vector2Int GetGridPos()
    {
        return new Vector2Int
            (
            Mathf.RoundToInt(transform.position.x / gridSize),
            Mathf.RoundToInt(transform.position.z / gridSize)
            );
    }
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (isPlaceable)
            {
                FindObjectOfType<TowerFactory>().PlaceTower(this);
            }
            else
            {
                print("Can't place here");
            }
        }        
    }

    public void SetTopColor(Color color)
    {
        MeshRenderer topMeshRenderer = transform.Find("Top").GetComponent<MeshRenderer>();
        topMeshRenderer.material.color = color;
    }

    /* private void SetIsExploredColor()
    {
        if (isExplored)
        {
           SetTopColor(exploredColor);
        }
        else
        {
            // do nothing
        }
    }
    */

    
    
}
