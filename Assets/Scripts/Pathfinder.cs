﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder : MonoBehaviour
{
    [SerializeField] Waypoint startWaypoint, endWaypoint;
    Dictionary<Vector2Int, Waypoint> grid = new Dictionary<Vector2Int, Waypoint>();
    Queue<Waypoint> queue = new Queue<Waypoint>();
    bool isRunning = true;
    Waypoint searchCentre; // the current searchCentre
    List<Waypoint> path = new List<Waypoint>();

    Vector2Int[] directions =
    {
        Vector2Int.up,
        Vector2Int.right,
        Vector2Int.down,
        Vector2Int.left
    };

    public List<Waypoint> GetPath()
    {
        if (path.Count == 0)
        {
            CalculatePath();
        }
        return path;     
    }

    private void CalculatePath()
    {
        LoadBlocks();
        // ColorStartEnd();
        BreadthFirstSearch();
        CreatePath();
    }

    private void CreatePath()
    {
        SetAsPath(endWaypoint);

        Waypoint previous = endWaypoint.exploredFrom;
        while (previous != startWaypoint)
        {
            // add intermediate waypoints
            SetAsPath(previous);
            previous = previous.exploredFrom;
        }

        // add start waypoint
        SetAsPath(startWaypoint);
        // reverse the list
        path.Reverse();
    }

    private void SetAsPath(Waypoint waypoint)
    {
        path.Add(waypoint);
        waypoint.isPlaceable = false;
    }

    private void BreadthFirstSearch()
    {
        queue.Enqueue(startWaypoint);

        while(queue.Count > 0 && isRunning)
        {
            searchCentre = queue.Dequeue();
            // print("Searching from " + searchCentre); // test and remove later
            StopIfEndFound();
            ExploreNeighbours();
            searchCentre.isExplored = true;
        }
    }

    private void StopIfEndFound()
    {
        if (searchCentre == endWaypoint)
        {
            // print("End node found"); // todo test and remove 
            isRunning = false;
        }
    }

    private void ExploreNeighbours()
    {
        if (!isRunning) { return; }

        foreach (Vector2Int direction in directions)
        {
            Vector2Int neighbourCoordinates = searchCentre.GetGridPos() + direction;
            if (grid.ContainsKey(neighbourCoordinates))
            {
                QueueNewNeighbours(neighbourCoordinates);
            }
        }

    }

    private void QueueNewNeighbours(Vector2Int neighbourCoordinates)
    {
        Waypoint neighbour = grid[neighbourCoordinates];
        if (neighbour.isExplored || queue.Contains(neighbour))
        {
            // do nothing
        }
            else
        {
            queue.Enqueue(neighbour);
            // print("Queuing " + neighbour); 
            neighbour.exploredFrom = searchCentre;
        }
    }

    private void LoadBlocks()
    {
        var waypoints = FindObjectsOfType<Waypoint>(); // loading each wp into dictionary
        foreach (Waypoint waypoint in waypoints)
        {
            //overlapping blocks?
            var gridPos = waypoint.GetGridPos();
            if (grid.ContainsKey(gridPos))
                {
                Debug.LogWarning("Overlapping block skipped" + waypoint);
                }
                else
                {
                grid.Add(gridPos, waypoint);
                // waypoint.SetTopColor(Color.red); TEST:OK call from WP.cs for top mesh
                }
            //add to dictionary
            // print("Loaded " + grid.Count + " blocks"); 
        }
    }

    private void ColorStartEnd()
    {
        startWaypoint.SetTopColor(Color.blue);
        endWaypoint.SetTopColor(Color.red);
    }

   
}
