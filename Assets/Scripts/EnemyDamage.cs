﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    [SerializeField] Transform parent;
    [SerializeField] int MoneyPerKill = 12;
    [SerializeField] int hitPoints = 10;
    [SerializeField] ParticleSystem hitParticlePrefab;
    [SerializeField] ParticleSystem deathParticlePrefab;
    [SerializeField] AudioClip enemyHitSFX;
    [SerializeField] AudioClip enemyDeathSFX;

    AudioSource myAudioSource; 
   
    // ScoreBoard scoreBoard;

    // Start is called before the first frame update
    void Start()
    {
        // AddNonTriggerBoxCollider();
        // scoreBoard = FindObjectOfType<ScoreBoard>();
        myAudioSource = GetComponent<AudioSource>();
    }

    /* private void AddNonTriggerBoxCollider()
    {
        Collider boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = false;
    }
    */
    void OnParticleCollision(GameObject other)
    {
        // print("target hit");
        ProcessHit();
        if (hitPoints <= 0) 
        {
            KillEnemy();
        }
    }

    private void ProcessHit()
    {
        // scoreBoard.ScoreHit(ScorePerKill);
        hitPoints = hitPoints - 1;
        // or hits--;
        hitParticlePrefab.Play();
        myAudioSource.PlayOneShot(enemyHitSFX);
    }
    

    public void KillEnemy()
    {
        // print("Target " + gameObject.name + " destroyed");
        var vfx = Instantiate(deathParticlePrefab, transform.position, Quaternion.identity);
        vfx.Play();

        /* no longer need in Unity update
         float destroyDelay = vfx.main.duration;
         Destroy(vfx.gameObject, destroyDelay);
        */

        AudioSource.PlayClipAtPoint(enemyDeathSFX, Camera.main.transform.position);

        Destroy(gameObject);
    }
}
