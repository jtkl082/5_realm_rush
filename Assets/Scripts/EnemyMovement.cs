﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    // [SerializeField] List<Waypoint> path; moved to pathfinder
    [Tooltip("in seconds")] [SerializeField] float movementPeriod = 1f;
    [SerializeField] ParticleSystem goalParticlePrefab;

    // Start is called before the first frame update
    void Start()
    {
        Pathfinder pathfinder = FindObjectOfType<Pathfinder>();
        var path = pathfinder.GetPath();
        StartCoroutine(FollowPath(path));
    }

    IEnumerator FollowPath(List<Waypoint> path)
    {
        // print("Start Patrol");
        foreach (Waypoint waypoint in path)
        {
            // print(waypoint.name); for testing
            transform.position = waypoint.transform.position;
            yield return new WaitForSeconds(movementPeriod);
        }
        // print("End patrol");
        Selfdestruct();
    }

    private void Selfdestruct()
    {
        var vfx = Instantiate(goalParticlePrefab, transform.position, Quaternion.identity);
        vfx.Play();
        Destroy(gameObject);
    }
}
