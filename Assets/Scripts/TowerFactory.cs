﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour
{
    [SerializeField] int towerLimit = 5;
    [SerializeField] Tower towerPrefab;
    [SerializeField] Transform towerParent;

    Queue<Tower> towerQueue = new Queue<Tower>();

    private void Update()
    {
                
    }

    public void PlaceTower(Waypoint baseWaypoint)
    {
         // todo change queue size
         // var towers = FindObjectsOfType<Tower>(); // alternate method that works
         int numTowers = towerQueue.Count; // no need "numberOfTowers++;" int numberOfTowers = 0;

        if (numTowers < towerLimit)
        {
            InstantiateTower(baseWaypoint);
        }
        else
        {
            MoveExistingTower(baseWaypoint);
        }
            
    }

    private void InstantiateTower(Waypoint baseWaypoint)
    {
        var newTower = Instantiate(towerPrefab, baseWaypoint.transform.position, Quaternion.identity);
        newTower.transform.parent = towerParent;
        baseWaypoint.isPlaceable = false;

        // set tower waypoints
        newTower.baseWaypoint = baseWaypoint;
        baseWaypoint.isPlaceable = false;

        towerQueue.Enqueue(newTower);
    }

    private void MoveExistingTower(Waypoint newBasewaypoint)
    {
        // take bottom tower off queue
        var oldTower = towerQueue.Dequeue();

        // set placeable flags
        oldTower.baseWaypoint.isPlaceable = true;
        newBasewaypoint.isPlaceable = false;

        // set the baseWaypoints
        oldTower.baseWaypoint = newBasewaypoint;
        oldTower.transform.position = newBasewaypoint.transform.position;

        // put the old tower on top of queue
        towerQueue.Enqueue(oldTower);
    }
}
